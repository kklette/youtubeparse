package youtubeparse

import (
	"testing"
)

func Test_Should_Parse_All_Formats(t *testing.T) {
	var formats = map[string]string{
		"http://youtube.com/watch?v=1234567890":                                                  "1234567890",
		"https://youtube.com/watch?v=1234567890":                                                 "1234567890",
		"http://www.youtube.com/watch?v=1234567890":                                              "1234567890",
		"https://www.youtube.com/watch?v=1234567890":                                             "1234567890",
		"http://youtu.be/1234567890":                                                             "1234567890",
		"https://youtu.be/1234567890":                                                            "1234567890",
		"http://www.youtu.be/1234567890":                                                         "1234567890",
		"https://www.youtu.be/1234567890":                                                        "1234567890",
		"http://www.youtube.com/watch?v=1234567890&feature=context&context=G2de15aaFAAAAAAAAAAA": "1234567890",
		"www.youtube.com/watch?v=1234567890":                                                     "1234567890",
		"youtube.com/watch?v=1234567890":                                                         "1234567890",
		"https://www.youtube.com/embed/oBMGP61Fidc":                                              "oBMGP61Fidc",
		"http://youtu.be/oBMGP61Fidc":                                                            "oBMGP61Fidc",
		"youtu.be/1234567890":                                                                    "1234567890",
		"https://www.youtube.com/watch?v=BX-7BlRc_a0":                                            "BX-7BlRc_a0",
	}

	for k, v := range formats {
		res := ExtractYoutubeId(k)
		if res != v {
			t.Errorf("Expected %s for %s got %s", v, k, res)
		}
	}
}

func Test_Should_Return_List_of_All_Ids(t *testing.T) {
	testString := `
	http://youtu.be/1234567890
	 is cool, but http://youtu.be/1234567893 is better`

	res := ExtractYoutubeIds(testString)

	if len(res) != 2 {
		t.Errorf("Got %d results. Expected 2", len(res))
		return
	}
	if res[0] != "1234567890" {
		t.Errorf("Expected first result 1234567890 got %s", res[0])
	}
	if res[1] != "1234567893" {
		t.Errorf("Expected first result 1234567893 got %s", res[1])
	}
}
