package youtubeparse

import (
	"regexp"
	"strings"
)

var YOUTUBE_REGEXP = regexp.MustCompile(`.*youtu(be.com|.be)\/(watch\?v=|embed/)?([a-zA-Z0-9\-_]+).*`)

// Returns the youtube id for a single match.
// Example:
// ExtractYoutubeId("youtube.com/watch?v=1234567890") => "1234567890"
func ExtractYoutubeId(s string) string {
	m := YOUTUBE_REGEXP.FindAllStringSubmatch(s, 2)
	if len(m) == 0 {
		return ""
	}
	return m[0][3]
}

// Returns the youtube ids for all matches in the given string
// Example:
// ExtractYoutubeId(`
//	youtube.com/watch?v=1234567890
//      youtube.com/watch?v=0987654321`) => ["1234567890", "0987654321"]
func ExtractYoutubeIds(s string) []string {
	res := make([]string, 0, 0)
	for _, word := range strings.Fields(s) {
		vid := ExtractYoutubeId(word)
		if len(vid) != 0 {
			res = append(res, vid)
		}
	}
	return res
}
