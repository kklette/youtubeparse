# Simple library for extracting youtube ids from links

[![Build Status](https://drone.io/bitbucket.org/kklette/youtubeparse/status.png)](https://drone.io/bitbucket.org/kklette/youtubeparse/latest)

See test file for example usage.
